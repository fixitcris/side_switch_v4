﻿Imports System.ComponentModel
Imports System.IO
Imports System.Net.Sockets
Imports System.Text
Imports System.Windows.Threading
Class MainWindow
    Dim WithEvents Timer1 As New DispatcherTimer
    Dim WithEvents Timer2 As New DispatcherTimer
    Dim WithEvents CheckConn As New BackgroundWorker
    Dim WithEvents ChangeSides As New BackgroundWorker
    Private Sub MainWindow_Initialized(sender As Object, e As EventArgs) Handles Me.Initialized
        CheckConn.WorkerSupportsCancellation = True
        ChangeSides.WorkerSupportsCancellation = True
        Timer1.Interval = New TimeSpan(0, 0, 0, 0.5)
        Timer2.Interval = New TimeSpan(0, 0, 10, 0)
        OneSide_BTN.IsEnabled = False
        TwoSide_BTN.IsEnabled = False
        Authenticated = False
        Line_Select = Nothing
        L1rec.Visibility = 1
        L2rec.Visibility = 1
        L3rec.Visibility = 1
        L4rec.Visibility = 1
    End Sub
    Private Sub MainWindow_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        logindone = False
        Timer1.Start()
    End Sub
    Private Sub Timers_BTN_Click(sender As Object, e As RoutedEventArgs) Handles Timers_BTN.Click
        If Authenticated = True Then
            UHT1_BTN.IsEnabled = False
            UHT2_BTN.IsEnabled = False
            UHT3_BTN.IsEnabled = False
            UHT4_BTN.IsEnabled = False
            TimersCC.Height = 180
            TimersCC.Width = 380
            TimersCC.Content = New Timers()
            TimersCC.Visibility = 0
        Else
            passwordCC.Height = 180
            passwordCC.Width = 380
            passwordCC.Content = New Password()
            passwordCC.Visibility = 0
        End If
        TimersBox = True
        pause = True
    End Sub

    Private Sub UHT1_BTN_Click(sender As Object, e As RoutedEventArgs) Handles UHT1_BTN.Click
        Line_Select = 1
        UHT2_BTN.IsEnabled = False
        UHT3_BTN.IsEnabled = False
        UHT4_BTN.IsEnabled = False
        SingleApplyDelay = My.Settings.L1ApplyDelay
        SingleApplyTime = My.Settings.L1ApplyTime
        DoubleApplyDelay = My.Settings.L1DoubleApplyDelay
        DoubleApplyTime = My.Settings.L1DoubleApplyTime
        SecondApplyDelay = My.Settings.L12ndApplyDelay
        SecondApplyTime = My.Settings.L12ndApplyTime
        L1f = True
        If CheckConn.IsBusy = False Then
            CheckConn.RunWorkerAsync()
        End If
        Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Status_Update))
    End Sub

    Private Sub UHT2_BTN_Click(sender As Object, e As RoutedEventArgs) Handles UHT2_BTN.Click
        Line_Select = 2
        UHT1_BTN.IsEnabled = False
        UHT3_BTN.IsEnabled = False
        UHT4_BTN.IsEnabled = False
        SingleApplyDelay = My.Settings.L2ApplyDelay
        SingleApplyTime = My.Settings.L2ApplyTime
        DoubleApplyDelay = My.Settings.L2DoubleApplyDelay
        DoubleApplyTime = My.Settings.L2DoubleApplyTime
        SecondApplyDelay = My.Settings.L22ndApplyDelay
        SecondApplyTime = My.Settings.L22ndApplyTime
        L2f = True
        If CheckConn.IsBusy = False Then
            CheckConn.RunWorkerAsync()
        End If
        Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Status_Update))
    End Sub
    Private Sub UHT3_BTN_Click(sender As Object, e As RoutedEventArgs) Handles UHT3_BTN.Click
        Line_Select = 3
        UHT1_BTN.IsEnabled = False
        UHT2_BTN.IsEnabled = False
        UHT4_BTN.IsEnabled = False
        SingleApplyDelay = My.Settings.L3ApplyDelay
        SingleApplyTime = My.Settings.L3ApplyTime
        DoubleApplyDelay = My.Settings.L3DoubleApplyDelay
        DoubleApplyTime = My.Settings.L3DoubleApplyTime
        SecondApplyDelay = My.Settings.L32ndApplyDelay
        SecondApplyTime = My.Settings.L32ndApplyTime
        L3f = True
        If CheckConn.IsBusy = False Then
            CheckConn.RunWorkerAsync()
        End If
        Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Status_Update))
    End Sub

    Private Sub UHT4_BTN_Click(sender As Object, e As RoutedEventArgs) Handles UHT4_BTN.Click
        Line_Select = 4
        UHT1_BTN.IsEnabled = False
        UHT2_BTN.IsEnabled = False
        UHT3_BTN.IsEnabled = False
        SingleApplyDelay = My.Settings.L4ApplyDelay
        SingleApplyTime = My.Settings.L4ApplyTime
        DoubleApplyDelay = My.Settings.L4DoubleApplyDelay
        DoubleApplyTime = My.Settings.L4DoubleApplyTime
        SecondApplyDelay = My.Settings.L42ndApplyDelay
        SecondApplyTime = My.Settings.L42ndApplyTime
        L4f = True
        If CheckConn.IsBusy = False Then
            CheckConn.RunWorkerAsync()
        End If
        Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Status_Update))
    End Sub
    Private Sub OneSide_BTN_Click(sender As Object, e As RoutedEventArgs) Handles OneSide_BTN.Click
        If OneSide_BTN.Background Is Blue Then
            If busy = False Then
                ChangeSides.RunWorkerAsync(argument:=1)
            End If
        End If
    End Sub
    Private Sub TwoSide_BTN_Click(sender As Object, e As RoutedEventArgs) Handles TwoSide_BTN.Click
        If TwoSide_BTN.Background Is Blue Then
            If busy = False Then
                ChangeSides.RunWorkerAsync(argument:=2)
            End If
        End If
    End Sub
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If Authenticated = True And logindone = False Then
            Button_txt.Text = "Timers"
            Login.Text = "Supervisor"
            Timer2.Start()
            Status = "Login Successful"
            Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Status_Update))
            logindone = True
            TimersBox = False
        ElseIf Authenticated = True Then
        Else
            Button_txt.Text = "Login"
            Login.Text = "Operator"
            Timers_BTN.IsEnabled = True
        End If
        If TimersBox = True And pause = False And Authenticated = False And incorrect = False Then
            Status = "Supervisor logged out"
            Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Status_Update))
            TimersBox = False
            logindone = False
        ElseIf TimersBox = True And pause = False And incorrect = True Then
            Status = "Incorrect password"
            Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Status_Update))
            TimersBox = False
        End If
        If TimersBox = False And CheckConn.IsBusy = False Then
            UHT1_BTN.IsEnabled = True
            UHT2_BTN.IsEnabled = True
            UHT3_BTN.IsEnabled = True
            UHT4_BTN.IsEnabled = True
        End If
    End Sub
    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        If Authenticated = True Then
            Authenticated = False
            logindone = False
            Status = "Automatically logged out"
            Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Status_Update))
        Else
        End If
        Timer2.Stop()
    End Sub

    Private Sub CheckConn_DoWork(sender As Object, e As DoWorkEventArgs) Handles CheckConn.DoWork
        Select Case Line_Select
            Case 1
                Target = My.Settings.Line1ip
            Case 2
                Target = My.Settings.Line2ip
            Case 3
                Target = My.Settings.Line3ip
            Case 4
                Target = My.Settings.Line4ip
        End Select
        XCLString = "!!a.parameter.twosidedapplication?" & vbCr
        Using Check As New TcpClient
            Status = "Checking connection to line " & Line_Select & " labeller..."
            Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Status_Update))
            Try
                If Check.Connected = False Then
                    Check.Connect(Target, Port)
                End If
                If Check.Connected = True Then
                    Dim Writer As New StreamWriter(Check.GetStream())
                    Dim stream As NetworkStream = Check.GetStream
                    Dim checkbytes As Byte() = Encoding.ASCII.GetBytes(Status_Check)
                    Dim XCLBytes As Byte()
                    stream.Write(checkbytes, 0, checkbytes.Length)
                    Console.WriteLine("sent to 2200: " & Status_Check)
                    Writer.Flush()
                    Check_response = ""
                    I = stream.Read(bytes, 0, bytes.Length)
                    Check_response = Encoding.ASCII.GetString(bytes, 0, I)
                    Console.WriteLine("received from 2200: " & Check_response)
                    If Check_response.Contains("00000000") Then
                        Status = "Line " & Line_Select & " communications OK"
                        XCLBytes = Encoding.ASCII.GetBytes(XCLString)
                        stream.Write(XCLBytes, 0, XCLBytes.Length)
                        Console.WriteLine("sent to 2200: " & XCLString)
                        'Writer.Flush()
                        Check_response = ""
                        I = stream.Read(bytes, 0, bytes.Length)
                        Check_response = Encoding.ASCII.GetString(bytes, 0, I)
                        Console.WriteLine("received from 2200: " & Check_response)
                        If Check_response.Contains("!0") Then
                            Sides = 1
                        ElseIf Check_response.Contains("!1") Then
                            Sides = 2
                        Else
                            Status = "Line " & Line_Select & " Printer fault"
                            Fault = True
                            Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Status_Update))
                            Exit Try
                            CheckConn.CancelAsync()
                            e.Cancel = True
                        End If
                        Select Case Line_Select
                            Case 1
                                L1f = False
                                L1Sides = Sides
                            Case 2
                                L2f = False
                                L2Sides = Sides
                            Case 3
                                L3f = False
                                L3Sides = Sides
                            Case 4
                                L4f = False
                                L4Sides = Sides
                        End Select
                        Status = "Line " & Line_Select & " set to " & Sides & " label/s"
                        Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Status_Update))
                    Else
                        Status = "Line " & Line_Select & " Printer fault"
                        Fault = True
                        Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Status_Update))
                        Exit Try
                        CheckConn.CancelAsync()
                        e.Cancel = True
                    End If
                End If
            Catch ex As Exception
                Select Case Line_Select
                    Case 1
                        L1f = True
                    Case 2
                        L2f = True
                    Case 3
                        L3f = True
                    Case 4
                        L4f = True
                End Select
                Status = "Line " & Line_Select & " communications fault: " & ex.Message
                Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Status_Update))
                'MsgBox("Line " & Line_Select & " comms fault: " & ex.Message)
                CheckConn.CancelAsync()
                e.Cancel = True
            End Try
            Check.Close()
        End Using
    End Sub

    Private Sub CheckConn_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles CheckConn.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            '' if BackgroundWorker terminated due to error
            MessageBox.Show(e.Error.Message)
            CheckConn.Dispose()
        ElseIf e.Cancelled Then
            CheckConn.Dispose()
        End If
        busy = False
    End Sub

    Private Sub ChangeSides_DoWork(sender As Object, e As DoWorkEventArgs) Handles ChangeSides.DoWork
        busy = True
        Dim value As Integer = e.Argument
        Console.WriteLine("Number of sides = " & value)
        Select Case Line_Select
            Case 1
                Target = My.Settings.Line1ip
            Case 2
                Target = My.Settings.Line2ip
            Case 3
                Target = My.Settings.Line3ip
            Case 4
                Target = My.Settings.Line4ip
        End Select
        Select Case value
            Case 1
                XCLString = "!!a.parameter.twosidedapplication!0" & vbCr & "!!a.parameter.applydelay!" & SingleApplyDelay & vbCr & "!!a.parameter.applytime!" & SingleApplyTime & vbCr & "!!a.parameter.returnsensor!0" & vbCr
            Case 2
                XCLString = "!!a.parameter.twosidedapplication!1" & vbCr & "!!a.parameter.applydelay!" & DoubleApplyDelay & vbCr & "!!a.parameter.applytime!" & DoubleApplyTime & vbCr & "!!a.parameter.secondapplydelay!" & SecondApplyDelay & vbCr & "!!a.parameter.secondapplytime!" & SecondApplyTime & vbCr & "!!a.parameter.returnsensor!1" & vbCr
        End Select
        Using Check As New TcpClient
            Try
                If Check.Connected = False Then
                    Check.Connect(Target, Port)
                End If
                If Check.Connected = True Then
                    Dim Writer As New StreamWriter(Check.GetStream())
                    Writer.Write(XCLString)
                    Writer.Flush()
                End If
            Catch ex As Exception
                Select Case Line_Select
                    Case 1
                        L1f = True
                    Case 2
                        L2f = True
                    Case 3
                        L3f = True
                    Case 4
                        L4f = True
                End Select
                Status = "Line " & Line_Select & " communications fault: " & ex.Message
                Dispatcher.Invoke(Windows.Threading.DispatcherPriority.Send, New Action(AddressOf Status_Update))
                ChangeSides.CancelAsync()
                e.Cancel = True
            End Try
            Check.Close()
        End Using
    End Sub
    Private Sub ChangeSides_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles ChangeSides.RunWorkerCompleted
        If e.Error IsNot Nothing Then
            '' if BackgroundWorker terminated due to error
            MessageBox.Show(e.Error.Message)
            ChangeSides.Dispose()
        ElseIf e.Cancelled Then
            ChangeSides.Dispose()
        Else
            ChangeSides.Dispose()
            CheckConn.RunWorkerAsync()
        End If
    End Sub
    Private Function Status_Update()
        Select Case Line_Select
            Case 0
                UHT1_BTN.Background = Blue
                UHT2_BTN.Background = Blue
                UHT3_BTN.Background = Blue
                UHT4_BTN.Background = Blue
                L1rec.Visibility = 1
                L2rec.Visibility = 1
                L3rec.Visibility = 1
                L4rec.Visibility = 1
                Timers_BTN.IsEnabled = False
                OneSide_BTN.IsEnabled = False
                TwoSide_BTN.IsEnabled = False
            Case 1 And L1f = False
                L1rec.Visibility = 0
                L2rec.Visibility = 1
                L3rec.Visibility = 1
                L4rec.Visibility = 1
                UHT1_BTN.Background = Green
                Timers_BTN.IsEnabled = True
                OneSide_BTN.IsEnabled = True
                TwoSide_BTN.IsEnabled = True
                Select Case L1Sides
                    Case 1
                        OneSide_BTN.Background = Green
                        TwoSide_BTN.Background = Blue
                    Case 2
                        OneSide_BTN.Background = Blue
                        TwoSide_BTN.Background = Green
                End Select
            Case 1 And L1f = True
                L1rec.Visibility = 0
                L2rec.Visibility = 1
                L3rec.Visibility = 1
                L4rec.Visibility = 1
                UHT1_BTN.Background = Red
                Timers_BTN.IsEnabled = False
                OneSide_BTN.IsEnabled = False
                TwoSide_BTN.IsEnabled = False
            Case 2 And L2f = False
                L1rec.Visibility = 1
                L2rec.Visibility = 0
                L3rec.Visibility = 1
                L4rec.Visibility = 1
                UHT2_BTN.Background = Green
                Timers_BTN.IsEnabled = True
                OneSide_BTN.IsEnabled = True
                TwoSide_BTN.IsEnabled = True
                Select Case L2Sides
                    Case 1
                        OneSide_BTN.Background = Green
                        TwoSide_BTN.Background = Blue
                    Case 2
                        OneSide_BTN.Background = Blue
                        TwoSide_BTN.Background = Green
                End Select
            Case 2 And L2f = True
                L1rec.Visibility = 1
                L2rec.Visibility = 0
                L3rec.Visibility = 1
                L4rec.Visibility = 1
                UHT2_BTN.Background = Red
                Timers_BTN.IsEnabled = False
                OneSide_BTN.IsEnabled = False
                TwoSide_BTN.IsEnabled = False
            Case 3 And L3f = False
                L1rec.Visibility = 1
                L2rec.Visibility = 1
                L3rec.Visibility = 0
                L4rec.Visibility = 1
                UHT3_BTN.Background = Green
                Timers_BTN.IsEnabled = True
                OneSide_BTN.IsEnabled = True
                TwoSide_BTN.IsEnabled = True
                Select Case L3Sides
                    Case 1
                        OneSide_BTN.Background = Green
                        TwoSide_BTN.Background = Blue
                    Case 2
                        OneSide_BTN.Background = Blue
                        TwoSide_BTN.Background = Green
                End Select
            Case 3 And L3f = True
                L1rec.Visibility = 1
                L2rec.Visibility = 1
                L3rec.Visibility = 0
                L4rec.Visibility = 1
                UHT3_BTN.Background = Red
                Timers_BTN.IsEnabled = False
                OneSide_BTN.IsEnabled = False
                TwoSide_BTN.IsEnabled = False
            Case 4 And L4f = False
                L1rec.Visibility = 1
                L2rec.Visibility = 1
                L3rec.Visibility = 1
                L4rec.Visibility = 0
                UHT4_BTN.Background = Green
                Timers_BTN.IsEnabled = True
                OneSide_BTN.IsEnabled = True
                TwoSide_BTN.IsEnabled = True
                Select Case L4Sides
                    Case 1
                        OneSide_BTN.Background = Green
                        TwoSide_BTN.Background = Blue
                    Case 2
                        OneSide_BTN.Background = Blue
                        TwoSide_BTN.Background = Green
                End Select
            Case 4 And L4f = True
                L1rec.Visibility = 1
                L2rec.Visibility = 1
                L3rec.Visibility = 1
                L4rec.Visibility = 0
                UHT4_BTN.Background = Red
                Timers_BTN.IsEnabled = False
                OneSide_BTN.IsEnabled = False
                TwoSide_BTN.IsEnabled = False
        End Select
        If Fault = True Then
            StatusBox.FontSize = 20
            Fault = False
        Else
            StatusBox.FontSize = 10
        End If
        If Status IsNot Nothing Then
            StatusBox.Text = Status
        End If
    End Function
End Class
