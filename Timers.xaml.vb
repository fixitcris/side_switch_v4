﻿Public Class Timers
    Private Sub Timers_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        Header.Text = "Line " & Line_Select & " timers"
        SNGApplyDelay.Text = SingleApplyDelay
        SNGApplyTime.Text = SingleApplyTime
        DUBApplyDelay.Text = DoubleApplyDelay
        DUBApplyTime.Text = DoubleApplyTime
        _2ndApplyDelay.Text = SecondApplyDelay
        _2ndApplyTime.Text = SecondApplyTime
    End Sub
    Private Sub Close_BTN_Click(sender As Object, e As RoutedEventArgs) Handles Close_BTN.Click
        Select Case Line_Select
            Case 1
                My.Settings.L1ApplyDelay = SNGApplyDelay.Text
                My.Settings.L1ApplyTime = SNGApplyTime.Text
                My.Settings.L1DoubleApplyDelay = DUBApplyDelay.Text
                My.Settings.L1DoubleApplyTime = DUBApplyTime.Text
                My.Settings.L12ndApplyDelay = _2ndApplyDelay.Text
                My.Settings.L12ndApplyTime = _2ndApplyTime.Text
            Case 2
                My.Settings.L2ApplyDelay = SNGApplyDelay.Text
                My.Settings.L2ApplyTime = SNGApplyTime.Text
                My.Settings.L2DoubleApplyDelay = DUBApplyDelay.Text
                My.Settings.L2DoubleApplyTime = DUBApplyTime.Text
                My.Settings.L22ndApplyDelay = _2ndApplyDelay.Text
                My.Settings.L22ndApplyTime = _2ndApplyTime.Text
            Case 3
                My.Settings.L3ApplyDelay = SNGApplyDelay.Text
                My.Settings.L3ApplyTime = SNGApplyTime.Text
                My.Settings.L3DoubleApplyDelay = DUBApplyDelay.Text
                My.Settings.L3DoubleApplyTime = DUBApplyTime.Text
                My.Settings.L32ndApplyDelay = _2ndApplyDelay.Text
                My.Settings.L32ndApplyTime = _2ndApplyTime.Text
            Case 4
                My.Settings.L4ApplyDelay = SNGApplyDelay.Text
                My.Settings.L4ApplyTime = SNGApplyTime.Text
                My.Settings.L4DoubleApplyDelay = DUBApplyDelay.Text
                My.Settings.L4DoubleApplyTime = DUBApplyTime.Text
                My.Settings.L42ndApplyDelay = _2ndApplyDelay.Text
                My.Settings.L42ndApplyTime = _2ndApplyTime.Text
        End Select
        My.Settings.Save()
        Width = 1
        Height = 1
        If Authenticated = True Then
            TimersBox = False
        End If
        pause = False
    End Sub

    Private Sub Logout_BTN_Click(sender As Object, e As RoutedEventArgs) Handles Logout_BTN.Click
        Authenticated = False
        Close_BTN_Click(Nothing, Nothing)
        pause = False
    End Sub
End Class
