﻿Imports System.Net.Security

Public Class Password

    Private Sub Password_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        PwordEntered = ""
        pword.Focus()
    End Sub
    Private Sub pword_PasswordChanged(sender As Object, e As RoutedEventArgs) Handles pword.PasswordChanged
        PwordEntered = pword.Password
    End Sub
    Private Sub Enter_BTN_Click(sender As Object, e As RoutedEventArgs) Handles Enter_BTN.Click
        PwordEntered = pword.Password
        If PwordEntered = "mituht" Then
            Width = 1
            Height = 1
            incorrect = False
            Authenticated = True
        Else
            incorrect = True
            Authenticated = False
            Width = 1
            Height = 1
        End If
        pause = False
    End Sub

    Private Sub Cancel_BTN_Click(sender As Object, e As RoutedEventArgs) Handles Cancel_BTN.Click
        Width = 1
        Height = 1
        Authenticated = False
        pause = False
    End Sub

    Private Sub pword_KeyDown(sender As Object, e As KeyEventArgs) Handles pword.KeyDown
        If e.Key = Key.Enter Then
            Enter_BTN_Click(Nothing, Nothing)
        ElseIf e.Key = Key.Escape Then
            Cancel_BTN_Click(Nothing, Nothing)
        End If
    End Sub

End Class
